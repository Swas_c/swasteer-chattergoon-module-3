import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter app',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter app Home Page'),
    );
  }
}

class MyHomePageState extends State<MyHomePage> {
  final _text = TextEditingController();
  bool _validate = false;

  @override
  void dispose() {
    _text.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(' login'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Error Showed if Field is Empty on Submit button Pressed'),
            TextField(
              controller: _text,
              decoration: InputDecoration(
                labelText: 'Enter login',
                errorText: _validate ? 'Value Can\'t Be Empty' : null,
              ),
            ),
            RaisedButton(
              onPressed: () {
                setState(() {
                  _text.text.isEmpty ? _validate = true : _validate = false;
                });
              },
              child: Text('Submit'),
              textColor: Colors.white,
              color: Colors.blueAccent,
            )
          ],
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++; //this is feature page
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:', //on feature page
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Tabs(refresh: () => setState(() {})),
            value == 0
                ? Center(child: Text("login", style: TextStyle(fontSize: 56)))
                : Container(),
            value == 1
                ? Center(
                    child: Text("home page", style: TextStyle(fontSize: 56)))
                : Container(),
            value == 2
                ? Center(
                    child: Text("feature page", style: TextStyle(fontSize: 56)))
                : Container(),
            value == 3
                ? Center(
                    child: Text("feature page", style: TextStyle(fontSize: 56)))
                : Container(),
            value == 4
                ? Center(
                    child: Text("feature page", style: TextStyle(fontSize: 56)))
                : Container(),
            value == 5
                ? Center(
                    child:
                        Text("change details", style: TextStyle(fontSize: 56)))
                : Container(),
          ],
        ),
      ),
    );
  }
}

class Tabs extends StatefulWidget {
  final Function refresh;

  Tabs({this.refresh});

  @override
  _TabsState createState() => _TabsState();
}

int value = 1;

class _TabsState extends State<Tabs> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: 24),
        MyTab(
            text: 'login',
            isSelected: value == 0,
            onTap: () => _updateValue(0)),
        MyTab(
            text: 'home page',
            isSelected: value == 1,
            onTap: () => _updateValue(1)),
        MyTab(
            text: 'feature page',
            isSelected: value == 2,
            onTap: () => _updateValue(2)),
        MyTab(
            text: 'feature page',
            isSelected: value == 2,
            onTap: () => _updateValue(3)),
        MyTab(
            text: 'feature page',
            isSelected: value == 2,
            onTap: () => _updateValue(4)),
        MyTab(
            text: 'change details',
            isSelected: value == 2,
            onTap: () => _updateValue(5)),
      ],
    );
  }

  void _updateValue(int newValue) {
    widget.refresh();
    setState(() {
      value = newValue;
    });
  }
}

class MyTab extends StatelessWidget {
  final String text;
  final bool isSelected;
  final Function onTap;

  const MyTab(
      {Key key, @required this.isSelected, @required this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
        onTap: onTap,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              text,
              style: TextStyle(
                fontSize: isSelected ? 16 : 14,
                color: isSelected ? Colors.black : Colors.grey,
                fontWeight: isSelected ? FontWeight.w600 : FontWeight.w500,
              ),
            ),
            Container(
              height: 6,
              width: 20,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                color: isSelected ? Color(0xFFFF5A1D) : Colors.white,
              ),
            )
          ],
        ),
      ),
    );
  }

  /*    @override
    Widget build(BuildContext context) {
        return
        
        TextFormField(
          controller: _usernameController,
          decoration: InputDecoration(
              hintText: "Username", errorText: _usernameError),
          style: TextStyle(fontSize: 18.0),
        ),
        Container(
          width: double.infinity,
          height: 50.0,
          child: RaisedButton(
            onPressed: validate,
            child: Text(
              "Login",
              style: TextStyle(color: Colors.white),
            ),
            color: Theme.of(context).primaryColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
          ),
        ),
        ...
    }

 

   // validate(){
    //    if(!validateStructure(_usernameController.text)){
    //        setState(() {
    //            _usernameError = emailError;
       ///         _passwordError = passwordError;
     //       });
            // show dialog/snackbar to get user attention.
     //       return;
  //      }
        // Continue 
  //  }
*/

}
